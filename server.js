
const express = require('express');
const app = express();
const port = 3000;
const { MongoClient, ObjectId } = require("mongodb");
  
const connectionString = "mongodb://root:root@localhost:27017";
const client = new MongoClient(connectionString);

const MONGO_ID_REGEX = /^([0-9a-fA-F]{12})\1*$/

app.use(express.json());


app.listen(port, function() {
    console.log('listening on http://localhost:3000')
  })

  app.get('/', (req, res) => {
    res.send('Hello World')
  })

  app.use(express.static('public'));

  app.get('/todos',async (req,res)=>{
      try{
        await client.connect();
        const db = client.db("tododb");
        const todos = db.collection("todos");
        const todo_list = await todos.find().toArray();
        console.log(todo_list)
        res.send(todo_list);
      }catch(e){
console.log(e);
      }finally{
          client.close()
      }
  })

  app.post('/todos', async (req, res, next) =>{
    const description = req.body.description
    if(!description){
      
        return res.status(400).send({error : "Description missing"})
    }else{
    try{
        await client.connect();
        const db = client.db("tododb");
        const todos = db.collection("todos");
        const {insertedId : _id} = await todos.insertOne({description});
        const todo = {
            _id ,
            description
        }
        console.log(todo)
        res.send(todo);
      }catch(e){
        next(e)
        console.log(e);
      }finally{
          client.close()
      }}
  })

  app.get('/todos/:id',async (req, res, next) =>{
    const {id} = req.params
    console.log(id)
    if(!id.match(MONGO_ID_REGEX)){
        res.status(400).send({error :  "ID not valid"})
    }
    try{
        const _id = new ObjectId(id);
        await client.connect();
        const db = client.db("tododb");
        const todos = db.collection("todos");
        const todo = await todos.findOne({_id});
        console.log(todo)
        if(todo){
            res.send(todo);
        }else{
            res.status(400).send({error :  "not find"})
        }
        
      }catch(e){
            console.log(e);
      }finally{
          client.close()
      }
  })

  app.put('/todos/:id', async (req, res, next) => {
    const id = req.params.id
    console.log(id)
    if(!id.match(MONGO_ID_REGEX)){
        res.status(400).send({error :  "ID not valid"})
    }
    try{
        const {description} = req.body
        const _id = new ObjectId(id);
        await client.connect();
        const db = client.db("tododb");
        const todos = db.collection("todos");
        const todo = await todos.findOneAndUpdate({_id}, {
            $set: {
                description 
            }
        });
        console.log(todo)
        if(todo){
            res.send(todo);
        }else{
            res.status(400).send({error :  "not find"})
        }
        
      }catch(e){
            console.log(e);
      }finally{
          client.close()
      }
  })

  app.put('/todos/:id', async (req, res, next) => {
    const id = req.params.id
    console.log(id)
    if(!id.match(MONGO_ID_REGEX)){
        res.status(400).send({error :  "ID not valid"})
    }
    try{
        const {description} = req.body
        const _id = new ObjectId(id);
        await client.connect();
        const db = client.db("tododb");
        const todos = db.collection("todos");
        const todo = await todos.findOneAndUpdate({_id}, {
            $set: {
                description 
            }
        });
        console.log(todo)
        if(todo){
            res.send(todo);
        }else{
            res.status(400).send({error :  "not find"})
        }
        
      }catch(e){
            console.log(e);
      }finally{
          client.close()
      }
  })

  app.delete('/todos/:id', async (req, res, next) => {
    const id = req.params.id
    console.log(id)
    if(!id.match(MONGO_ID_REGEX)){
        res.status(400).send({error :  "ID not valid"})
    }
    try{
        const _id = new ObjectId(id);
        await client.connect();
        const db = client.db("tododb");
        const todos = db.collection("todos");
        const {deletedCount} = await todos.deleteOne({_id});
        if(deletedCount){
            res.send();
        }else{
            res.status(404).send({error :  "not found"})
        }
        
      }catch(e){
            console.log(e);
      }finally{
          client.close()
      }
  })

  app.use(function(error, req, res, next) {
    console.error(error.stack);
    res.status(500).send('internal server error');
  });

  






